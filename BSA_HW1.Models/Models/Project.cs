﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_HW1.Models.Models
{
    public class Project
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public User User { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Descriprion { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
    }
}
