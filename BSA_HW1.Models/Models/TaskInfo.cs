﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_HW1.Models.Models
{
    public class TaskInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
