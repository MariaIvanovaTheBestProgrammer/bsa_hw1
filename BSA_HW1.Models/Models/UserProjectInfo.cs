﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_HW1.Models.Models
{
    public class UserProjectInfo
    {
        public User User { get; set; }
        public Project LastCreatedProject { get; set; }
        public int AllTasksCount { get; set; }
        public int CancelledOrInProgressTasksCount { get; set; }
        public Task LongestTask { get; set; }
    }
}
