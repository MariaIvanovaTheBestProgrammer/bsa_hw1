﻿using BSA_HW1.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA_HW1.Menu.Interfaces
{
    interface IDataHttpService
    {
        Task<IEnumerable<Project>> GetProjects();
        Task<IEnumerable<Models.Models.Task>> GetTasks();
        Task<IEnumerable<Team>> GetTeams();
        Task<IEnumerable<User>> GetUsers();
    }
}
