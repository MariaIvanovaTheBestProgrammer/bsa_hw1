﻿using BSA_HW1.Menu.Services;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace BSA_HW1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            DataHttpService dataHttpService = new DataHttpService();

            var users = await dataHttpService.GetUsers();
            var teams = await dataHttpService.GetTeams();
            var projects = await dataHttpService.GetProjects();
            var tasks = await dataHttpService.GetTasks();
            HierarchyService hierarchyService = new HierarchyService(projects, tasks, users, teams);
            var usersWithTeams = hierarchyService.GetUsersWithRelatedData();
            var projectsWithTasks = hierarchyService.GetProjectsWithRelatedData();
            var teamsWithUsers = hierarchyService.GetTeamsWithRelatedData();
            LinqService linqService = new LinqService(projectsWithTasks, tasks, usersWithTeams, teamsWithUsers);
            var firstTask = linqService.GetProjectToTasksCountDictionary(26);
            var secondTask = linqService.GetUserTasks(81);
            var thirdTask = linqService.GetFinishedTaskIdName(23);
            var forthTask = linqService.GetSortedTeamsInfo();
            var fifthTask = linqService.GetUsersSortedByName();
            var sixthTask = linqService.GetUserProjectInfo(29);
            //var tmp = projectsWithTasks.Where(p => p.AuthorId == 26).Select( p => new {Id = p.Id, Name = "tmp"});
        }
    }
}
