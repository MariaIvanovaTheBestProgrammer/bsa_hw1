﻿using BSA_HW1.Menu.Interfaces;
using BSA_HW1.Models.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BSA_HW1.Menu.Services
{
    public class DataHttpService : IDataHttpService
    {
        private readonly HttpClient client;

        public DataHttpService()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri("https://bsa21.azurewebsites.net/api/")
            };
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync("Projects");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Project>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }

        public async Task<IEnumerable<Models.Models.Task>> GetTasks()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync("Tasks");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Models.Models.Task>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }

        public async Task<IEnumerable<Team>> GetTeams()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync("Teams");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Team>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync("Users");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<User>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
    }
}
