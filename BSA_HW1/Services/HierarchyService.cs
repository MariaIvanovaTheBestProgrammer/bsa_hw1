﻿using BSA_HW1.Models.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW1.Menu.Services
{
    public class HierarchyService
    {
        private readonly IEnumerable<Project> projects;
        private readonly IEnumerable<Task> tasks;
        private readonly IEnumerable<User> users;
        private readonly IEnumerable<Team> teams;

        public HierarchyService(
                IEnumerable<Project> projects,
                IEnumerable<Task> tasks,
                IEnumerable<User> users,
                IEnumerable<Team> teams
            )
        {
            this.projects = projects;
            this.tasks = tasks;
            this.users = users;
            this.teams = teams;
        }

        public IEnumerable<User> GetUsersWithRelatedData()
        {
            return users.Join(
                              teams,
                              u => u.TeamId,
                              t => t.Id,
                              (u, t) =>
                                {
                                    u.Team = t;
                                    return u;
                                }
                        )
                        .GroupJoin(
                                    tasks,
                                    u => u.Id,
                                    t => t.PerformerId,
                                    (u, t) =>
                                    {
                                        u.Tasks = t;
                                        return u;
                                    }
                        )
                        .GroupJoin(
                                    projects,
                                    u => u.Id,
                                    p => p.AuthorId,
                                    (u, p) =>
                                    {
                                        u.Projects = p.GroupJoin(
                                                                  tasks,
                                                                  p => p.Id,
                                                                  t => t.ProjectId,
                                                                  (p, t) =>
                                                                  {
                                                                        p.Tasks = t;
                                                                        return p;
                                                                  }
                                                                );
                                        return u;
                                    });
        }

        public IEnumerable<Project> GetProjectsWithRelatedData()
        {
            return projects.GroupJoin(
                                      tasks,
                                      p => p.Id,
                                      t => t.ProjectId,
                                      (p, t) =>
                                        {
                                            p.Tasks = t;
                                            return p;
                                        }
                            )
                            .Join(
                                    teams.GroupJoin(
                                                    users,
                                                    t => t.Id,
                                                    u => u.TeamId,
                                                    (t, u) =>
                                                    {
                                                        t.Users = u;
                                                        return t;
                                                    }
                                                    ),
                                    p => p.TeamId,
                                    t => t.Id,
                                    (p,t) =>
                                    {
                                        p.Team = t;
                                        return p;
                                    }
                            );
        }

        public IEnumerable<Team> GetTeamsWithRelatedData()
        {
            return teams.GroupJoin(
                                    users,
                                    t => t.Id,
                                    u => u.TeamId,
                                    (t, u) =>
                                        {
                                            t.Users = u;
                                            return t;
                                        }
                                    );
        }
    }
}
