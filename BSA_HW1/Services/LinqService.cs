﻿using BSA_HW1.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW1.Menu.Services
{
    public class LinqService
    {
        private readonly IEnumerable<Project> projects;
        private readonly IEnumerable<Task> tasks;
        private readonly IEnumerable<User> users;
        private readonly IEnumerable<Team> teams;

        public LinqService(
                IEnumerable<Project> projects,
                IEnumerable<Task> tasks,
                IEnumerable<User> users,
                IEnumerable<Team> teams
            )
        {
            this.projects = projects;
            this.tasks = tasks;
            this.users = users;
            this.teams = teams;
        }
        
        //first task
        public Dictionary<Project, int> GetProjectToTasksCountDictionary(int authorId)
        {
            return projects.Where(p => p.AuthorId == authorId)
                           .ToDictionary(x => x, x => x.Tasks?.Count() ?? 0);
        }

        //second task
        public IEnumerable<Task> GetUserTasks(int userId)
        {
            return tasks.Where(t => t.PerformerId == userId && t.Name.Length < 45 );
        }

        //third task
        public IEnumerable<TaskInfo> GetFinishedTaskIdName(int userId)
        {
            return tasks.Where(t => t.PerformerId == userId)
                        .Where(t => t.TaskState == TaskState.Done && t.FinishedAt?.Year == 2021)
                        .Select(t => new TaskInfo { Id = t.TaskId, Name = t.Name });            
        }

        //forth task
        public IEnumerable<TeamInfo> GetSortedTeamsInfo()
        {
            return teams.Where(t => t.Users.All(u => DateTime.Now.Year - u.BirthDay.Year >= 10))
                        .Select(t => new TeamInfo
                        {
                            Id = t.Id,
                            Name = t.Name,
                            Users = t.Users.OrderByDescending(u => u.RegisteredAt)
                        });
        }

        //fifth task
        public IEnumerable<User> GetUsersSortedByName()
        {
            return users.OrderBy(u => u.FirstName)
                        .Select(u => {
                                        u.Tasks = u.Tasks.OrderByDescending(t => t.Name.Length);
                                        return u;
                                      });
        } 

        //sixth task
        public UserProjectInfo GetUserProjectInfo(int userId)
        {
            return users.Where(u => u.Id == userId)
                        .Select(u => new UserProjectInfo
                        {
                            User = u,
                            LastCreatedProject = u.Projects.OrderByDescending(p => p.CreatedAt).FirstOrDefault(),
                            AllTasksCount = u.Projects.OrderByDescending(p => p.CreatedAt)
                                                           .FirstOrDefault()
                                                           ?.Tasks?.Count() ?? 0,
                            CancelledOrInProgressTasksCount = u.Tasks.Where(t => t.TaskState != TaskState.Done || t.TaskState == TaskState.Canceled).Count(),
                            LongestTask = u.Tasks.OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt).FirstOrDefault()
                        })
                        .SingleOrDefault();
        }

        //seventh task
        public IEnumerable<ProjectInfo> GetProjectInfo()
        {
            return projects.Where(p => p.Descriprion.Length > 20 || p.Tasks.Count() < 3)
                           .Select(p => new ProjectInfo
                            {
                                Project = p,
                                LongestDescriptionTask = p.Tasks.OrderByDescending(t => t.Description).FirstOrDefault(),
                                ShortestNameTask = p.Tasks.OrderBy(t => t.Name).FirstOrDefault(),
                                ProjectTeamCount = p.Team.Users.Count()
                            });
        }
    }
}
